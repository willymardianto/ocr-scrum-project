package com.example.tesseractoncf.dao;

import com.example.tesseractoncf.model.io.request.SplitBill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface SplitBillDao extends JpaRepository<SplitBill, BigInteger> {

}
