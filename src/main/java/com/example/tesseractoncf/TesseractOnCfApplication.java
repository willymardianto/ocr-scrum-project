package com.example.tesseractoncf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesseractOnCfApplication {

    public static void main(String[] args) {
        System.setProperty("org.bytedeco.javacpp.maxphysicalbytes", "0");
        System.setProperty("org.bytedeco.javacpp.maxbytes", "0");
        setProxy();
        SpringApplication.run(TesseractOnCfApplication.class, args);
    }

    public static void setProxy() {
//        System.setProperty("https.proxyUser", "u068476");
//        System.setProperty("https.proxyPassword", "Stefanus@5");
//        System.setProperty("http.proxyPort", "8080");
//        System.setProperty("https.proxyPort", "8080");
//        System.setProperty("http.proxyHost", "kpproxygsit");
//        System.setProperty("https.proxyHost", "kpproxygsit");

//        System.setProperty("http.proxyPort", "8080");
//        System.setProperty("https.proxyPort", "8080");
//        System.setProperty("http.proxyHost", "10.17.10.42");
//        System.setProperty("https.proxyHost", "10.17.10.42");
    }
}
